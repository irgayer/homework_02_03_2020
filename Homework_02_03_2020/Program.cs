﻿using System;
using System.Data;
using Homework_02_03_2020.Adapter;
using Homework_02_03_2020.Bridge;
using Homework_02_03_2020.Facade;
using AppContext = Homework_02_03_2020.Bridge.AppContext;

namespace Homework_02_03_2020
{
    class Program
    {
        static void Main(string[] args)
        {
            //Adapter
            var train = new Train();
            ICar car = new Adapter.Adapter(train);

            Console.WriteLine(car.Color);
            
            //Facade
            var system1 = new PlusMinusSystem();
            var system2 = new MultiplyDivideSystem();
            
            var facade = new Facade.Facade(system1, system2);

            string result = facade.Operation();
            Console.WriteLine(result);
            
            
            var context = new AppContext();
            var repository = new EFStringRepository(context);
            
            repository.AddString("Hello");
            repository.AddString("World");
            repository.AddString("!");

            foreach (var str in repository.Strings)
            {
                Console.WriteLine(str); 
            }
        }
    }
}