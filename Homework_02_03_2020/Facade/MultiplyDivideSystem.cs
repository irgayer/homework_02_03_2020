﻿namespace Homework_02_03_2020.Facade
{
    public class MultiplyDivideSystem
    {
        public string Operation1()
        {
            return "Multiply!\n";
        }

        public string Operation2()
        {
            return "Divide!\n";
        }
    }
}