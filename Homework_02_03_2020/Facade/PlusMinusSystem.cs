﻿namespace Homework_02_03_2020.Facade
{
    public class PlusMinusSystem
    {
        public string Operation1()
        {
            return "Plus!\n";
        }

        public string Operation2()
        {
            return "Minus!\n";
        }
    }
}