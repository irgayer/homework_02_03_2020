﻿namespace Homework_02_03_2020.Facade
{
    public class Facade
    {
        protected PlusMinusSystem system1;
        protected MultiplyDivideSystem system2;

        public Facade(PlusMinusSystem system1, MultiplyDivideSystem system2)
        {
            this.system1 = system1;
            this.system2 = system2;
        }

        public string Operation()
        {
            var result = string.Empty;

            result += system1.Operation1();
            result += system2.Operation1();
            result += system1.Operation2();
            result += system2.Operation2();

            return result;
        }
    }
}