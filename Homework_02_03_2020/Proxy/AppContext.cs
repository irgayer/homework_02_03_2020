﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Homework_02_03_2020.Bridge
{
    public class AppContext // : DbContext
    {
        /*
        public AppContext()
        {
            base...    
        }*/
        
        public /*DbSet*/ ICollection<string> Strings { get; set; } = new List<string>();
    }
}