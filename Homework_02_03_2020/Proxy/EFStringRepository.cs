﻿using System.Collections.Generic;
using System.Linq;

namespace Homework_02_03_2020.Bridge
{
    public class EFStringRepository : IStringRepository
    {
        AppContext context;

        public EFStringRepository(AppContext context)
        {
            this.context = context;
        }


        public List<string> Strings
        {
            get => context.Strings.ToList();
        }
        public void AddString(string str)
        {
            context.Strings.Add(str);
            //context.SaveChanges();
        }
    }
}