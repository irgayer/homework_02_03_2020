﻿using System.Collections.Generic;

namespace Homework_02_03_2020.Bridge
{
    public interface IStringRepository
    {
        public List<string> Strings { get; }
        public void AddString(string str);
    }
}