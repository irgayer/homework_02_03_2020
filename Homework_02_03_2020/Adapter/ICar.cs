﻿namespace Homework_02_03_2020.Adapter
{
    public interface ICar
    {
        public float Velocity { get; set; }
        public string Color { get; set; }
    }
}