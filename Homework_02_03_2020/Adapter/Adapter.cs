﻿namespace Homework_02_03_2020.Adapter
{
    public class Adapter : ICar
    {
        readonly Train _train;

        public Adapter(Train train)
        {
            _train = train;
        }

        public float Velocity { get; set; }
        public string Color { get => _train.GetSpicificColor(); set => Color = value; }
    }
}